# rocker-geospatial-getspatialdata

This image is based on [rocker/geospatial](https://hub.docker.com/r/rocker/geospatial/), a Docker-based Geospatial toolkit for R, built on versioned Rocker images. It adds support to `getSpatialData`, an R package in an early development stage that ultimately aims to provide homogeneous function bundles to query, download, prepare and transform various kinds of spatial datasets from open sources, e.g. Satellite sensor data, higher-level environmental data products etc. [Read more about it here](https://github.com/16EAGLE/getSpatialData).

# Usage

Check out [this page](https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image).


